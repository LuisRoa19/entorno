# Puppet manifest for my PHP dev machine

# Edit local /etc/hosts files to resolve some hostnames used on your application.
host { 'localhost':
  ensure => 'present',
  target => '/etc/hosts',
  ip => '127.0.0.1',
  host_aliases => ['mysql', 'memcached']
}

# Miscellaneous packages.
$misc_packages = ['vim-enhanced','telnet','zip','unzip','git','screen','libssh2','libssh2-devel', 'gcc', 'gcc-c++', 'autoconf', 'automake']

package { $misc_packages: ensure => latest }

class{'apache':}

apache::vhost { 'myMpwar.dev':
          port          => '80',
          docroot       => '/var/www',
          
    }

apache::vhost { 'myMpwar.prod':
          port          => '80',
          docroot       => '/var/www',
          
    }


class { '::mysql::server':
        root_password           => 'root',
        remove_default_accounts => true,
    }

mysql::db { 'mympwar_exam':
      user     => 'user',
      password => 'root',
      host     => 'localhost',
      grant    => ['SELECT', 'INSERT', 'UPDATE', 'DELETE'],
    }

    mysql::db { 'mympwar_backup':
      user     => 'user',
      password => 'root',
      host     => 'localhost',
      grant    => ['SELECT', 'INSERT', 'UPDATE', 'DELETE'],
    }


# PHP
$php_version = '54'

# remi_php55 requires the remi repo as well
if $php_version == '55' {
  $yum_repo = 'remi-php55'
  include ::yum::repo::remi_php55
}
# remi_php56 requires the remi repo as well
elsif $php_version == '56' {
  $yum_repo = 'remi-php56'
  ::yum::managed_yumrepo { 'remi-php56':
    descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch - PHP 5.6',
    mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/php56/mirror',
    enabled        => 1,
    gpgcheck       => 1,
    gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
    gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',
    priority       => 1,
  }
}
# version 5.4
elsif $php_version == '54' {
  $yum_repo = 'remi'
  include ::yum::repo::remi
}

class { 'php':
  version => 'latest',
  require => Yumrepo[$yum_repo]
}

php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap' ]: }

# memcached
include memcached

# Ensure Time Zone and Region.
class { 'timezone':
    timezone => 'Europe/Madrid',
}

#NTP
class { '::ntp':
  servers => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
  package_ensure => 'latest'
}

file{"/var/www/index.php":
    ensure => present,
    content => "<?php echo 'Hello World. Sistema operativo ' . php_uname('s') .' ' .php_uname('v') ",
  }

  file{"/var/www/info.php":
    ensure => present,
    content => "<?php phpinfo();",
  }

  file_line{"Open port 8080":
    path => "/etc/sysconfig/iptables",
    line => "-A INPUT -p tcp -m tcp --dport 8080 -j ACCEPT",
  }

  file_line{"Open port 443":
    path => "/etc/sysconfig/iptables",
    line => "-A INPUT -p tcp -m tcp --dport 443 -j ACCEPT",
  }
