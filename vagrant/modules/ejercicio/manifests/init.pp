class ejercicio {
  file { '/web/myMpwar.dev/index.php':
    ensure => file,
    replace => true,
    content => 'Hello world. sistema operativo <$nombre> <$version>';
  }
  file { '/web/myMpwar.dev/info.php':
    ensure => file,
    replace => true,
    content => '<?php phpinfo()';
  }
}

